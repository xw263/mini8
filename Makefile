rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version             #rust compiler
	cargo --version             #rust package manager
	rustfmt --version           #rust code formatter
	rustup --version            #rust toolchain manager
	clippy-driver --version     #rust linter

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

lint:
	cargo clippy --quiet

build:
	cargo build --release

test:
	cargo test -- --show-output >> test.md

release:
	cargo build --release

extract:
	cargo run extract

transform_load:
	cargo run transform_load

create:
	cargo run query "INSERT INTO Birth (year, month, day_of_month, day_of_week, births) VALUES ( 2000, 1, 1, 6, 9083);"

read:
	cargo run query "SELECT * FROM Birth WHERE year = 2000;"

update:
	cargo run query "UPDATE Birth SET year=2000, month=1, day_of_month=1, day_of_week=1, births=9083 WHERE id=1;"

delete:
	cargo run query "DELETE FROM Birth WHERE id=3;"

all: format lint test create read update delete



