[![pipeline status](https://gitlab.com/xw263/mini8/badges/main/pipeline.svg)](https://gitlab.com/xw263/mini8/-/commits/main)

# mini_roject8

## Purpose
The goal of this project is to create Rust command line of tool with testing. For this project, it creates the ETL with Rust and develop command line tool. 

## Steps
1. run `cargo init`
2. create `lib.rs` where includes extract, query, and transform_load
3. Have testing in the `main.rs`
4. Update `Makefile`
5. Update `.gitlab-ci.yml`

## Pipeline
![Screenshot_2024-03-31_at_10.54.36_PM](/uploads/eed03bb117734fad4ab8f2c61bfd0d74/Screenshot_2024-03-31_at_10.54.36_PM.png)


## References
https://github.com/nogibjj/mini_project8_xueqing_wu
